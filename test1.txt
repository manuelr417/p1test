# Add car 1
curl -X POST -i -H "Content-Type: application/json" -d '{"carId" : 1, "carBrand" : "Toyota", "carModel" : "Highlander", "carModelOption" : "XLE", "carPrice" : 36000}' http://localhost:8080/cardealer/cars/add

# Add car 2
curl -X POST -i -H "Content-Type: application/json" -d '{"carId" : 3, "carBrand" : "Toyota", "carModel" : "Highlander", "carModelOption" : "Limited", "carPrice" : 52000}' http://localhost:8080/cardealer/cars/add

# Add car 3
curl -X POST -i -H "Content-Type: application/json" -d '{"carId" : 2, "carBrand" : "Hyundai", "carModel" : "Tucson", "carModelOption" : "SE", "carPrice" : 25000}' http://localhost:8080/cardealer/cars/add

# Add car 4
curl -X POST -i -H "Content-Type: application/json" -d '{"carId" : 4, "carBrand" : "Honda", "carModel" : "Fit", "carModelOption" : "LE", "carPrice" : 15000}' http://localhost:8080/cardealer/cars/add

# Add car 5
curl -X POST -i -H "Content-Type: application/json" -d '{"carId" : 5, "carBrand" : "Ford", "carModel" : "F-150", "carModelOption" : "Raptor", "carPrice" : 95000}' http://localhost:8080/cardealer/cars/add

# Add car 6
curl -X POST -i -H "Content-Type: application/json" -d '{"carId" : 6, "carBrand" : "Ford", "carModel" : "F-150", "carModelOption" : "Limited", "carPrice" : 55000}' http://localhost:8080/cardealer/cars/add

# Add car 7
curl -X POST -i -H "Content-Type: application/json" -d '{"carId" : 8, "carBrand" : "RAM", "carModel" : "1500", "carModelOption" : "BigHorn", "carPrice" : 65000}' http://localhost:8080/cardealer/cars/add

# Get all cars
curl -X GET -i http://localhost:8080/cardealer/cars

# Get car with id 1
curl -X GET -i http://localhost:8080/cardealer/cars/1

# Get car with id 4
curl -X GET -i http://localhost:8080/cardealer/cars/4

# Get car with id 20
curl -X GET -i http://localhost:8080/cardealer/cars/20



